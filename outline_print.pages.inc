<?php

// $Id$



function outline_print_pages($node) {

  if (!isset($node->book)) {
    // The node is not part of any book - redirect
    drupal_goto('');
  }

	// Get parent node
	if ($node->book['bid'] != $node->nid) {
		$node_parent = node_load($node->book['bid']);
	}
	else {
		$node_parent = $node;
	}
  
	// Set the parent node of outline for other functions.
  outline_print_parent_node($node_parent->nid);

	$subtree_data = book_menu_subtree_data($node_parent->book);	
	$subtree = array_shift($subtree_data);
	drupal_set_title($subtree['link']['title']);
	$tree = $subtree['below']; 

	foreach ($tree as $item) {
		$nid = $item['link']['nid'];
		$settings = outline_print_node_settings($nid);								

		if (!empty($settings['print_option']) && $settings['print_option'] != 'exclude') {
			$content .= outline_print_process_node($nid, $settings['print_option'], $settings['view'], $settings['display']);
		}
	}
	
	$path = path_to_theme() . '/logo-print.png';
	$print_logo = file_exists($path);
	$logo = $print_logo ? base_path() . $path : theme_get_setting('logo');

	$html = theme('outline_print', $content, $logo);
  $html = drupal_final_markup($html);
  print $html;
}

/**
 * $action is View to process node with
 */
function outline_print_process_node($nid, $print_option = 'node', $view = '', $display = '') {
	$node = node_load($nid);
	$class = preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. form_clean_id(drupal_strtolower($node->title)));
 
	$content .= '<h2>' . check_plain($node->title) . '</h2>';
	  
	if ($print_option == 'node') {
		// Add node as is.
		$content .= node_view($node, FALSE, FALSE, TRUE);
	}
	elseif (!empty($view) && !empty($display)) {
		// Use View to get node content.
		$view_class = ' page-view-' . check_plain($view);
	  $view = views_get_view($view);
	  $content .= $view->preview($display, array($nid));
	}
	
	$content = '<div class="' . $class . ' page-node-' . $nid . $view_class . '">' . $content . '</div>'; 

	return $content;
}