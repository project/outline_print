<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language_code; ?>" xml:lang="<?php print $language_code; ?>">
  <head>
    <?php print $head; ?>
    <?php print $base_href; ?>
    <title><?php print $head_title; ?></title>
    <?php print $scripts; ?>
    <?php print $sendtoprinter; ?>
    <?php print $robots_meta; ?>
    <?php print $favicon; ?>
    <?php print $styles; ?>
  </head>
  <body>
    <div class="outline-print-logo">
			<?php if (!empty($logo)): ?>
	      <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
	        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
	      </a>
	    <?php endif; ?> 
		</div>

    <h1 class="print-title"><?php print $title; ?></h1>
    <div class="print-content"><?php print $content; ?></div>
    <div class="print-source_url"><?php print $source_url; ?></div>
  </body>
</html>
